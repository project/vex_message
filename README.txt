
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

The VEX Message module provides a popup window for Drupal using external VEX - a
modern dialog library.

REQUIREMENTS
-------------

 * Filter (Drupal core)

INSTALLATION
-------------

Download and copy the 'vex_message' module directory in to your Drupal 'modules'
directory as usual.

CONFIGURATION
-------------

To configure VEX Message, go to Administration > Configuration > User Interface
> VEX Message. And add text which should be displayed in the popup window
